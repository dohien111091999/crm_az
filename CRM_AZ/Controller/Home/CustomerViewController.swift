//
//  CustomerViewController.swift
//  CRM_AZ
//
//  Created by vmio vmio on 3/15/19.
//  Copyright © 2019 CRM_AZ. All rights reserved.
//

import UIKit

class CustomerViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    let dataArray = ["Khách hàng", "Liên hệ", "Cơ hội", "Báo giá", "Hợp đồng"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellCustomer", for: indexPath) as! CustomerTableViewCell
            cell.nameLabel.text = dataArray[indexPath.row]
            cell.photoImage.image = UIImage(named: "heart")
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedCell:UITableViewCell = tableView.cellForRow(at: indexPath)!
        selectedCell.contentView.backgroundColor = UIColor.red
    }
    
}
