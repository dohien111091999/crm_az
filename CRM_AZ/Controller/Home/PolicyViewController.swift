//
//  PolicyViewController.swift
//  CRM_AZ
//
//  Created by vmio vmio on 3/15/19.
//  Copyright © 2019 CRM_AZ. All rights reserved.
//

import UIKit

class PolicyViewController: UIViewController {

    @IBOutlet weak var informationCardView: UIView!
    @IBOutlet weak var informationCustomerView: UIView!
    @IBOutlet weak var discountView: UIView!
    @IBOutlet weak var trackPointsView: UIView!
    @IBOutlet weak var customerOpinionView: UIView!
    @IBOutlet weak var historyView: UIView!
    @IBOutlet weak var historyOutView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        informationCardView.layer.cornerRadius = 5
        informationCardView.layer.borderColor = UIColor.gray.cgColor
        informationCardView.layer.borderWidth = 1
        
        informationCustomerView.layer.cornerRadius = 5
        informationCustomerView.layer.borderColor = UIColor.gray.cgColor
        informationCustomerView.layer.borderWidth = 1
        discountView.layer.cornerRadius = 5
        trackPointsView.layer.cornerRadius = 5
        customerOpinionView.layer.cornerRadius = 5
        historyView.layer.cornerRadius = 5
        historyOutView.layer.cornerRadius = 5

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
