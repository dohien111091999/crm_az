//
//  CustomerInformationController.swift
//  CRM_AZ
//
//  Created by vmio vmio on 3/14/19.
//  Copyright © 2019 CRM_AZ. All rights reserved.
//

import UIKit
class HomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
   
    let dataArray = ["Sản phẩm", "Khách hàng", "Chính sách", "Bán hàng", "Kho hàng", "Công việc"]
    
    @IBOutlet weak var logoutButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        logoutButton.layer.cornerRadius = 25
        // Do any additional setup after loading the view.
    }
    
    @IBAction func logoutButton(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
       @IBAction func unwindToContact(segue: UIStoryboardSegue) { }
}

extension HomeViewController {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellproduct", for: indexPath) as! HomeTableViewCell
        
        cell.nameLabel.text = dataArray[indexPath.row]
        cell.photoImage.image = UIImage(named: "heart")
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedCell:UITableViewCell = tableView.cellForRow(at: indexPath)!
        selectedCell.contentView.backgroundColor = UIColor.red
        
        switch indexPath.row {
        case 0:
            performSegue(withIdentifier: "selectedProduct", sender: self)
            break
        case 1:
            performSegue(withIdentifier: "selectedCustomer", sender: self)
            break
        case 2:
            performSegue(withIdentifier: "selectedPolicy", sender: self)
            break
        default:
            print("don't selected")
        }
    }
}
