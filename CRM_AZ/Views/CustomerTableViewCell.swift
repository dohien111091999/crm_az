//
//  CustomerTableViewCell.swift
//  CRM_AZ
//
//  Created by vmio vmio on 3/15/19.
//  Copyright © 2019 CRM_AZ. All rights reserved.
//
import UIKit
import Foundation

class CustomerTableViewCell: UITableViewCell {
    
    @IBOutlet weak var photoImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
}
